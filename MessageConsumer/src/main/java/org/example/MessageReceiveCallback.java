package org.example;

import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.Delivery;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class MessageReceiveCallback implements DeliverCallback {
    private static final Logger LOG = LogManager.getLogger();
    public static final long MAX_MESSAGE_AGE_NANO_SECONDS = 10 * 1000 * 1000 * 1000L;
    private static final String TIMESTAMP_JSON_KEY = "timestamp_nanoseconds";
    private static final String COUNTER_JSON_KEY = "counter";

    private final Storage storage;
    private final MessageHandler messageHandler;

    public MessageReceiveCallback(Storage storage, MessageHandler messageHandler) {
        this.storage = storage;
        this.messageHandler = messageHandler;
    }

    @Override
    public void handle(String consumerTag, Delivery delivery) {
        var messageString = new String(delivery.getBody(), StandardCharsets.UTF_8);
        LOG.debug("Message received:" + messageString);
        var message = extractMessageFromString(messageString);

        if (message != null) {
            handleMessage(message);
        }
    }

    private Message extractMessageFromString(String messageString) {
        Message message = null;

        try {
            var jsonMessage = new JSONObject(messageString);
            var timestamp = jsonMessage.getLong(TIMESTAMP_JSON_KEY);
            var counter = jsonMessage.getInt(COUNTER_JSON_KEY);
            message = new Message(timestamp, counter);
        } catch (JSONException jsonException) {
            LOG.error(jsonException);
        }

        return message;
    }

    private void handleMessage(Message message) {
        var currentTime = System.nanoTime();
        if (currentTime - message.timestampNanoSeconds() > MAX_MESSAGE_AGE_NANO_SECONDS) {
            LOG.debug("Message dropped: " + message);
            return;
        }

        if (isEven(message.counter())) {
            LOG.debug("Storing message: " + message);
            storage.storeMessage((message));
        } else {
            int updatedCounter = message.counter() + 1;
            var jsonMessage = createJSONFromMessage(new Message(message.timestampNanoSeconds(), updatedCounter));

            LOG.debug("Putting Message back into queue: " + message);
            sendJSONMessage(jsonMessage);
        }
    }

    private boolean isEven(int number) {
        return number % 2 == 0;
    }

    private JSONObject createJSONFromMessage(Message message) {
        var json = new JSONObject();
        json.put(TIMESTAMP_JSON_KEY, message.timestampNanoSeconds());
        json.put(COUNTER_JSON_KEY, message.counter());

        return json;
    }

    private void sendJSONMessage(JSONObject jsonMessage) {
        var message = jsonMessage.toString();

        try {
            messageHandler.sendMessage(message);
        } catch (IOException e) {
            LOG.error(e);
        }
    }
}

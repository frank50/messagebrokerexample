package org.example;

import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;

public interface MessageHandler {
    void sendMessage(String message) throws IOException;

    void receiveMessages(DeliverCallback deliverCallback) throws IOException;
}

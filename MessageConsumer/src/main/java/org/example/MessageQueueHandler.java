package org.example;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;

public class MessageQueueHandler implements MessageHandler {

    private Channel channel;
    private String queueName;

    public MessageQueueHandler(String hostName, String queueName) throws Exception {
        this.queueName = queueName;

        ConnectionFactory factory = createConnectionFactory(hostName);
        Connection connection = factory.newConnection();

        setupChannelWithQueue(connection);
    }

    public void sendMessage(String message) throws IOException {
        channel.basicPublish("", queueName, null, message.getBytes());
    }

    public void receiveMessages(DeliverCallback deliverCallback) throws IOException {
        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });
    }


    private ConnectionFactory createConnectionFactory(String hostName) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(hostName);

        return factory;
    }

    private void setupChannelWithQueue(Connection connection) throws IOException {
        channel = connection.createChannel();
        channel.queueDeclare(queueName, false, false, false, null);
    }


}

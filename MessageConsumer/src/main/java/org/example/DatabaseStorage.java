package org.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseStorage implements Storage {
    private static final Logger LOG = LogManager.getLogger();
    private static final String DATABASE_BASE_URL = "jdbc:postgresql://";
    private static final String TABLE_NAME = "messages";

    private final Connection conn;

    public DatabaseStorage(String databaseHostName, String databasePort, String databaseName, String databaseUser, String databasePassword) throws SQLException {
        var databaseURL = DATABASE_BASE_URL + databaseHostName + ":" + databasePort + "/" + databaseName;
        conn = DriverManager.getConnection(databaseURL, databaseUser, databasePassword);
    }

    public boolean storeMessage(Message message) {
        try {
            var st = conn.prepareStatement("INSERT INTO " + TABLE_NAME + " VALUES(?, ?);");

            st.setLong(1, message.timestampNanoSeconds());
            st.setInt(2, message.counter());

            st.execute();
            st.close();
            return true;
        } catch (SQLException e) {
            LOG.error(e);
            return false;
        }
    }
}

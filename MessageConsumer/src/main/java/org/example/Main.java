package org.example;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

public class Main {

    public static void main(String[] args) throws Exception {
        Properties properties = loadProperties();

        MessageQueueHandler messageQueueHandler = createMessageQueueHandler(properties);
        DatabaseStorage databaseStorage = createDatabaseStorage(properties);

        messageQueueHandler.receiveMessages(new MessageReceiveCallback(databaseStorage, messageQueueHandler));
    }

    private static Properties loadProperties() throws IOException {
        String configFilePath = "MessageConsumer/src/main/resources/config.properties";
        FileInputStream propsInput = new FileInputStream(configFilePath);

        Properties properties = new Properties();
        properties.load(propsInput);
        return properties;
    }

    private static MessageQueueHandler createMessageQueueHandler(Properties properties) throws Exception {
        var host = properties.getProperty("MSG_QUEUE_HOST");
        var queueName = properties.getProperty("MSG_QUEUE_NAME");
        return new MessageQueueHandler(host, queueName);
    }

    private static DatabaseStorage createDatabaseStorage(Properties properties) throws SQLException {
        var host = properties.getProperty("DB_HOST");
        var port = properties.getProperty("DB_PORT");
        var name = properties.getProperty("DB_NAME");
        var user = properties.getProperty("DB_USER");
        var pw = properties.getProperty("DB_PASSWORD");
        return new DatabaseStorage(host, port, name, user, pw);
    }
}
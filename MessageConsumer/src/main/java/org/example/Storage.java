package org.example;

public interface Storage {
    boolean storeMessage(Message message);
}

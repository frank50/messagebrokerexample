import com.rabbitmq.client.Delivery;
import org.example.Message;
import org.example.MessageHandler;
import org.example.MessageReceiveCallback;
import org.example.Storage;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class MessageReceiveCallBackTest {
    @Mock
    Storage storage;
    @Mock
    MessageHandler messageHandler;
    @Mock
    Delivery delivery;

    private MessageReceiveCallback messageReceiveCallback;

    @Before
    public void before() {
        messageReceiveCallback = new MessageReceiveCallback(storage, messageHandler);
    }

    @Test
    public void testHandleDoesStore() throws IOException {
        long time = System.nanoTime();
        int counter = 0;

        String result = "{\"timestamp_nanoseconds\":" + time + ",\"counter\":" + counter +"}";
        when(delivery.getBody()).thenReturn((result).getBytes(StandardCharsets.UTF_8));

        messageReceiveCallback.handle("", delivery);
        verify(storage).storeMessage(new Message(time, counter));
        verify(messageHandler, never()).sendMessage(anyString());
    }

    @Test
    public void testHandleDoesPutBack() throws IOException {
        long time = System.nanoTime();
        int counter = 1;

        String result = "{\"timestamp_nanoseconds\":" + time + ",\"counter\":" + counter +"}";
        when(delivery.getBody()).thenReturn((result).getBytes(StandardCharsets.UTF_8));

        int updatedCounter = 2;
        String updatedJsonString = "{\"timestamp_nanoseconds\":" + time + ",\"counter\":" + updatedCounter +"}";

        messageReceiveCallback.handle("", delivery);
        verify(messageHandler).sendMessage(updatedJsonString);
        verify(storage, never()).storeMessage(any());
    }

    @Test
    public void testHandleDiscards() throws IOException {
        long time = System.nanoTime() - MessageReceiveCallback.MAX_MESSAGE_AGE_NANO_SECONDS; // create message that is too old
        int counter = 1;

        String result = "{\"timestamp_nanoseconds\":" + time + ",\"counter\":" + counter +"}";
        when(delivery.getBody()).thenReturn((result).getBytes(StandardCharsets.UTF_8));

        messageReceiveCallback.handle("", delivery);
        verify(messageHandler, never()).sendMessage(anyString());
        verify(storage, never()).storeMessage(any());
    }
}

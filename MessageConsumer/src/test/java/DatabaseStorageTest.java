import org.example.DatabaseStorage;
import org.example.Message;
import org.junit.Before;
import org.junit.Test;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static org.junit.Assert.assertTrue;

@Testcontainers
public class DatabaseStorageTest {

    private static final String DATABASE_NAME = "TriforkTask";
    private static final String DATABASE_USER = "postgres";
    private static final String DATABASE_PASSWORD = "1";
    private static final int DEFAULT_POSTGRES_PORT = 5432;

    private Connection conn;
    private String host;
    private String port;

    @Container
    private PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres")
            .withDatabaseName(DATABASE_NAME)
            .withUsername(DATABASE_USER)
            .withPassword(DATABASE_PASSWORD);

    @Before
    public void before() throws SQLException {
        postgreSQLContainer.start();

        establishDatabaseConnection();
        prepareTable();
    }

    private void establishDatabaseConnection() throws SQLException {
        conn = DriverManager.getConnection(postgreSQLContainer.getJdbcUrl(), DATABASE_USER, DATABASE_PASSWORD);
        host = postgreSQLContainer.getHost();
        port = Integer.toString(postgreSQLContainer.getMappedPort(DEFAULT_POSTGRES_PORT));
    }

    private void prepareTable() throws SQLException {
        var statement = conn.createStatement();
        statement.execute("CREATE TABLE messages(time_stamp bigint, counter int);");
    }

    @Test
    public void testStore() throws SQLException {
        DatabaseStorage db = new DatabaseStorage(host, port, DATABASE_NAME, DATABASE_USER, DATABASE_PASSWORD);
        int timestamp = 1234;
        int counter = 3;

        db.storeMessage(new Message(timestamp, counter));

        var st = conn.createStatement();
        var isResult = st.execute("SELECT FROM messages WHERE time_stamp = 1234 AND counter = 3;");
        assertTrue(isResult);
    }
}

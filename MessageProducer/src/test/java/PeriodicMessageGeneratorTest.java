import org.example.MessageHandler;
import org.example.PeriodicMessageGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class PeriodicMessageGeneratorTest {
    private static final int DELAY_MILLIS = 5*1000;
    @Mock
    MessageHandler messageHandler;

    @Test
    public void testMessageGeneration() throws IOException {
        PeriodicMessageGenerator periodicMessageGenerator = new PeriodicMessageGenerator(messageHandler, DELAY_MILLIS);
        Thread t = new Thread(periodicMessageGenerator);
        t.start();

        verify(messageHandler, after(1000).times(1)).sendMessage(anyString());
        verify(messageHandler, after(6*1000).times(2)).sendMessage(anyString());

        verify(messageHandler).sendMessage(contains("counter\":0"));
        verify(messageHandler).sendMessage(contains("counter\":1"));
    }
}

package org.example;

import java.io.IOException;

public interface MessageHandler {
    void sendMessage(String message) throws IOException;
}

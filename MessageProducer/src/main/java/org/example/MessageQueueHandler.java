package org.example;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class MessageQueueHandler implements MessageHandler {
    private static final Logger LOG = LogManager.getLogger();
    private Channel channel;
    private String queueName;

    public MessageQueueHandler(String hostName, String queueName) throws Exception {
        this.queueName = queueName;

        ConnectionFactory factory = createConnectionFactory(hostName);
        Connection connection = factory.newConnection();

        setupChannelWithQueue(connection);
    }

    public void sendMessage(String message) throws IOException {
        channel.basicPublish("", queueName, null, message.getBytes());
        LOG.debug("Message sent: " + message);
    }

    private ConnectionFactory createConnectionFactory(String hostName) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(hostName);

        return factory;
    }

    private void setupChannelWithQueue(Connection connection) throws IOException {
        channel = connection.createChannel();
        channel.queueDeclare(queueName, false, false, false, null);
    }
}

package org.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.io.IOException;

public class PeriodicMessageGenerator implements Runnable {
    private static final Logger LOG = LogManager.getLogger();
    private static final String TIMESTAMP_JSON_KEY = "timestamp_nanoseconds";
    private static final String COUNTER_JSON_KEY = "counter";

    private final MessageHandler messageHandler;
    private final long delayMillis;

    private int counter = 0;


    public PeriodicMessageGenerator(MessageHandler messageHandler, int delayMillis) {
        this.messageHandler = messageHandler;
        this.delayMillis = delayMillis;
    }

    public void run() {
        while(true) {
            generateAndSendMessage();
            counter++;

            try {
                Thread.sleep(delayMillis);
            } catch (InterruptedException e) {
                LOG.error(e);
            }
        }
    }

    private void generateAndSendMessage() {
        var timestamp = System.nanoTime();
        var message = new Message(timestamp, counter);
        var jsonMessage = createJSONFromMessage(message);

        sendJSONMessage(jsonMessage);
    }

    private JSONObject createJSONFromMessage(Message message) {
        var json = new JSONObject();
        json.put(TIMESTAMP_JSON_KEY, message.timestampNanoSeconds());
        json.put(COUNTER_JSON_KEY, message.counter());

        return json;
    }

    private void sendJSONMessage(JSONObject jsonMessage) {
        var message = jsonMessage.toString();

        try {
            messageHandler.sendMessage(message);
        } catch (IOException e) {
            LOG.error(e);
        }
    }
}

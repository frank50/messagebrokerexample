package org.example;

public record Message(long timestampNanoSeconds, int counter) {
}

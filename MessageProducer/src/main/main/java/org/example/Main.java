package org.example;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Main {

    public static void main(String[] args) throws Exception {
        Properties properties = loadProperties();
        MessageQueueHandler messageQueueHandler = createMessageQueueHandler(properties);

        int delayMillis = Integer.parseInt(properties.getProperty("MESSAGE_GENERATION_DELAY_MILLIS"));
        var periodicMessageGenerator = new PeriodicMessageGenerator(messageQueueHandler, delayMillis);

        new Thread(periodicMessageGenerator).start();
    }

    private static Properties loadProperties() throws IOException {
        String configFilePath = "src/main/resources/config.properties";
        FileInputStream propsInput = new FileInputStream(configFilePath);

        Properties properties = new Properties();
        properties.load(propsInput);
        return properties;
    }

    private static MessageQueueHandler createMessageQueueHandler(Properties properties) throws Exception {
        var host = properties.getProperty("MSG_QUEUE_HOST");
        var queueName = properties.getProperty("MSG_QUEUE_NAME");
        return new MessageQueueHandler(host, queueName);
    }
}